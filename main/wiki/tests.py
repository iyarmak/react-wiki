from datetime import datetime
import json
from django.test import Client, TestCase
from .models import Article, Archive


class ArticleListAPITest(TestCase):
    def setUp(self):
        self.client = Client()

    @classmethod
    def setUpTestData(cls):
        for _ in range(0, 2):
            cls.article = Article()
            cls.article.title = 'title' + str(_)
            cls.article.body = 'body' + str(_)
            cls.article.created_at = datetime.now()
            cls.article.save()

            cls.archive = Archive()
            cls.archive.title = 'archive-title' + str(_)
            cls.archive.body = 'archive-body' + str(_)
            cls.archive.created_at = datetime.now()
            cls.archive.article = cls.article
            cls.archive.save()

    def test_get_articles(self):
        response = self.client.get('/api/articles')
        response_content = json.loads(response.content)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response_content.get('articles')), 2)
        self.assertEqual(response_content.get('articles')[0].get('title'), 'title1')


class ArticleAPITest(TestCase):
    def setUp(self):
        self.client = Client()

    @classmethod
    def setUpTestData(cls):
        for _ in range(0, 2):
            cls.article = Article()
            cls.article.title = 'title' + str(_)
            cls.article.body = 'body' + str(_)
            cls.article.created_at = datetime.now()
            cls.article.save()

            cls.archive = Archive()
            cls.archive.title = 'archive-title' + str(_)
            cls.archive.body = 'archive-body' + str(_)
            cls.archive.created_at = datetime.now()
            cls.archive.article = cls.article
            cls.archive.save()

    def test_get_article(self):
        response = self.client.get('/api/article/1/')
        response_content = json.loads(response.content)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response_content.get('article').get('id'), 1)

    def test_create_article(self):
        response = self.client.post('/api/article/', {'title': 'title3', 'body': 'body3'})
        response_content = json.loads(response.content)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response_content.get('article').get('title'), 'title3')

    def test_edit_article(self):
        self.client.post('/api/article/1/', {'title': 'title_edited', 'body': 'body3'})
        response = self.client.get('/api/article/1/')
        response_content = json.loads(response.content)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response_content.get('article').get('title'), 'title_edited')
