from rest_framework.response import Response
from .models import Article, Archive
from rest_framework.views import APIView
from django.db import transaction


# Create your views here.


class ArticleListAPIView(APIView):

    def get(self, request):
        articles = Article.objects.order_by('-created_at').all()
        return Response({
            'articles': [article.as_data() for article in articles],
        })


class ArticleAPIView(APIView):
    def get(self, request, id):
        article_data = None
        error = None
        try:
            archive = Archive.objects.filter(article_id=id). \
                          order_by('-created_at')[0:1]. \
                          get()
            article_data = archive.as_data()
        except Exception as e:
            error = e
        return Response({
            'article': article_data,
            'error': error if error else ''
        })

    @transaction.atomic
    def post(self, request, id):
        article_data = None
        error = None
        try:
            if not id:
                article = Article()
            else:
                article = Article.objects.get(id=id)

            article.title = request.data.get("title")
            article.body = request.data.get("body")
            article.save()

            archive = Archive()
            archive.title = request.data.get("title")
            archive.body = request.data.get("body")
            archive.article = article
            archive.save()

            article_data = archive.as_data()
        except Exception as e:
            error = e

        return Response({
            'article': article_data,
            'error': error if error else ''
        })
