from django.db import models
from django.forms.models import model_to_dict


# Create your models here.

class Article(models.Model):
    """
    """
    title = models.CharField(max_length=100, default=None)
    body = models.TextField(default=None)
    created_at = models.DateTimeField(auto_now_add=True)

    def as_data(self):
        """Return the Product as a dict suitable for passing as kwargs.
        """
        data = model_to_dict(self)
        return data


class Archive(models.Model):
    """
    """
    article = models.ForeignKey(Article, on_delete=models.CASCADE, blank=True, null=True)
    title = models.CharField(max_length=100, default=None)
    body = models.TextField(default=None)
    created_at = models.DateTimeField(auto_now_add=True)

    def as_data(self):
        """Return the Product as a dict suitable for passing as kwargs.
        """
        data = model_to_dict(self)
        return data
