from django.urls import path, re_path
from . import views

urlpatterns = [
    path('api/articles', views.ArticleListAPIView.as_view()),
    re_path(r'^api/article/((?P<id>\d+)/)?$', views.ArticleAPIView.as_view())
]