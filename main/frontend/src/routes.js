import React from 'react';
import App from "./components/App";
import NewArticle from "./components/new-article";
import {NavBar} from "./components/nav-bar";
import {Article} from "./components/article";
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";


export default function () {
  return (
    <Router>
      <div>
        <NavBar/>
        <Switch>
          <Route exact path="/" component={App}/>
          <Route path="/article/new" component={NewArticle}/>
          <Route path="/article/:articleId" component={Article}/>
        </Switch>
      </div>
    </Router>
  );
};