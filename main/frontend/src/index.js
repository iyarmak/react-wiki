import React from "react";
import ReactDOM from "react-dom";
import 'bootstrap/dist/css/bootstrap.css';
import RouterMain from './routes';

const wrapper = document.getElementById("app");
wrapper ? ReactDOM.render(<RouterMain />, wrapper) : null;