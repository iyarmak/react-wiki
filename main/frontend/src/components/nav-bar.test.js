import React from 'react';
import {NavBar} from './nav-bar';
import {shallow} from 'enzyme';

describe('NavBar component', () => {
  it('NavbarToggler isOpen false', () => {
    const wrapper = shallow(<NavBar/>);
    const isOpen = wrapper.state().isOpen;
    expect(isOpen).toEqual(false);
  })
});