import React, {Component} from 'react';
import {Media, Container} from 'reactstrap';
import {Link} from "react-router-dom";

export default class Articles extends Component {
  constructor(props) {
    super(props);
    this.state = {
      articles: []
    }
  }

  componentDidMount() {
    const headers = new Headers({
      "Content-Type": "application/x-www-form-urlencoded"
    });
    const init = {
      method: 'GET',
      headers: headers,
    };

    fetch('/api/articles', init)
      .then((response) => {
        return response.json();
      })
      .then((response) => {
        if (response.error) {
          console.log(response.error.message);
        }

        if (response.articles) {
          this.setState({articles: response.articles})
        }
      });
  }

  render() {
    return (
      <Container>
        {this.state.articles.map((article) => {
          return (
            <Media className="mt-2" key={`article-${article.id}`}>
              <Media body>
                <Media heading>
                  <Link to={`/article/${article.id}`}>{article.title}</Link>
                </Media>
                {article.body}
              </Media>
            </Media>
          );
        })}
      </Container>
    );
  }
}