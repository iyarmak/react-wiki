import React, {Component} from 'react';
import {Edit} from './edit';
import {Button} from 'reactstrap';

export default class NewArticle extends Component {
  constructor(props) {
    super(props);

    this.state = {
      title: "",
      body: ""
    };
  }

  handleSave = () => {
    const headers = new Headers({
      "Content-Type": "application/x-www-form-urlencoded"
    });

    const init = {
      method: 'POST',
      headers: headers,
      body: "title=" + encodeURIComponent(this.state.title) + "&body=" + encodeURIComponent(this.state.body)
    };

    fetch('/api/article/', init)
      .then((response) => {
        return response.json();
      })
      .then((response) => {
        if (response.error)
          console.log(response.error);
        else {
          this.setState({
            body: response.article.body,
            title: response.article.title
          });
          this.props.history.push('/');
        }
      });
  };

  handleChange = (e) => {
    if (e.target.name === 'title') {
      this.setState({title: e.target.value});
    }

    if (e.target.name === 'body') {
      this.setState({body: e.target.value});
    }
  };

  render() {
    return (
      <div>
        <Edit handleInputChange={this.handleChange}
              title={this.state.title}
              body={this.state.body}/>
        <Button color="success" className="mt-5" onClick={this.handleSave}>
          Save
        </Button>
      </div>


    )
  }
}