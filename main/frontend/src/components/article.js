import React, {Component} from 'react';
import {Media, Container, Row, Col, Button} from 'reactstrap';
import {Edit} from './edit';

export class Article extends Component {
  constructor(props) {
    super(props);

    this.state = {
      editMode: false,
      title: "",
      body: ""
    };
  }

  handleEdit = () => {
    this.setState({
      editMode: !this.state.editMode
    });
  };

  handleSave = () => {
    const headers = new Headers({
      "Content-Type": "application/x-www-form-urlencoded"
    });

    const init = {
      method: 'POST',
      headers: headers,
      body: "title=" + encodeURIComponent(this.state.title) + "&body=" + encodeURIComponent(this.state.body)
    };

    fetch('/api/article/' + this.props.match.params.articleId +'/', init)
      .then((response) => {
        return response.json();
      })
      .then((response) => {
        if (response.error)
          console.log(response.error);
        else {
          this.setState({
            body: response.article.body,
            title: response.article.title,
            editMode: !this.state.editMode
          })
        }
      });
  };

  handleChange = (e) => {
    if (e.target.name === 'title') {
      this.setState({title: e.target.value});
    }

    if (e.target.name === 'body') {
      this.setState({body: e.target.value});
    }
  };

  componentDidMount() {
    const myHeaders = new Headers({
      "Content-Type": "application/x-www-form-urlencoded",
      "x-access-token": window.localStorage.getItem('userToken')
    });
    const myInit = {
      method: 'GET',
      headers: myHeaders,
    };

    fetch('/api/article/' + this.props.match.params.articleId, myInit)
      .then((response) => {
        return response.json();
      })
      .then((response) => {
        if (response.error.error)
          console.log(response.error.message);
        else {
          this.setState({body: response.article.body, title: response.article.title})
        }
        this.setState({loading: false});
      });
  }


  render() {
    return (
      <Container>
        <Row>
          <Col xs="10">
            {this.state.editMode ?
              <Edit handleInputChange={this.handleChange}
                    title={this.state.title}
                    body={this.state.body}/> :
              (<Media>
                <Media body>
                  <Media heading>
                    {this.state.title}
                  </Media>
                  {this.state.body}
                </Media>
              </Media>)
            }
          </Col>
          <Col xs="2">
            {this.state.editMode ?
              (<Button color="success" className="mt-5" onClick={this.handleSave}>
                Save
              </Button>) :
              (<Button color="success" className="mt-5" onClick={this.handleEdit}>
                Edit
              </Button>)}

          </Col>
        </Row>
      </Container>
    )
  }
}