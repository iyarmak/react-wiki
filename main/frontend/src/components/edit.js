import React, {Component} from 'react';
import {Form, FormGroup, Label, Input} from 'reactstrap';

export class Edit extends Component {
  render() {
    const {handleInputChange, title, body} = this.props;
    return (
      <Form>
        <FormGroup>
          <Label for="titleId">Title</Label>
          <Input type="text" name="title" id="titleId"
                 placeholder="title"
                 value={title}
                 onChange={event => handleInputChange(event)}
          />
        </FormGroup>
        <FormGroup>
          <Label for="bodyId">Body</Label>
          <Input type="textarea"
                 name="body"
                 value={body}
                 id="bodyId"
                 onChange={event => handleInputChange(event)}/>
        </FormGroup>
      </Form>
    )
  }
}